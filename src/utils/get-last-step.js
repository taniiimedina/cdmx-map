export const getLastStep = (array) => {
  if (!array) return
  return array[array.length - 1].steps[array[array.length - 1].steps.length - 1]
}
