export const itinerary = {
  dest: 'Morelos',
  itinerary: [
    {
      line: 'Línea 2',
      segment: 1,
      steps: [
        { coords: '19.4447058,-99.1673774', index: 1, name: 'Normal' },
        {
          coords: '19.441782,-99.1611063',
          index: 2,
          name: 'San Cosme',
        },
        { coords: '19.4397232,-99.1555005', index: 3, name: 'Revolución' },
        {
          coords: '19.4373152,-99.1471374',
          index: 4,
          name: 'Hidalgo',
        },
        { coords: '19.4361922,-99.1419393', index: 5, name: 'Bellas Artes' },
      ],
    },
    {
      line: 'Línea 8',
      segment: 2,
      steps: [
        {
          coords: '19.4361922,-99.1419393',
          index: 1,
          name: 'Bellas Artes',
        },
        { coords: '19.4427583,-99.1392624', index: 2, name: 'Garibaldi' },
      ],
    },
    {
      line: 'Línea B',
      segment: 3,
      steps: [
        {
          coords: '19.4427583,-99.1392624',
          index: 1,
          name: 'Garibaldi',
        },
        { coords: '19.4433754,-99.1313499', index: 2, name: 'Lagunilla' },
        {
          coords: '19.4425205,-99.1233194',
          index: 3,
          name: 'Tepito',
        },
        { coords: '19.4389745,-99.1182554', index: 4, name: 'Morelos' },
      ],
    },
  ],
  origin: 'Normal',
  status: 200,
}
