export const metroStations = {
  stations: [
    'Observario',
    'Tacubaya',
    'Garibaldi',
    'Necaxa',
    'Test 1',
    'Test 2',
    'Test 3',
    'Test 4',
  ],
}

export const metrobusStations = {
  stations: [
    'La Raza',
    'Coyuya',
    'Balderas',
    'Juárez',
    'Ermita',
    'Universidad',
  ],
}
