import Map from './components/Map'
import MapProvider from './components/MapProvider'

function App() {
  return (
    <MapProvider>
      <Map />
    </MapProvider>
  )
}

export default App
