import SearchPanel from './SearchPanel'
import StepsPanel from './StepsPanel'
import { useContext, useState } from 'react'
import Tab from './Tab'
import Icon from './Icon'
import { ArrowDown } from '../static/icons/ArrowDown'
import { MapContext } from '../context/MapContext'
import { useMediaPredicate } from 'react-media-hook'
import { mediaToDesktop } from '../utils/mediaquery'

const ControlUi = ({ route, handleSetRoute, colors }) => {
  const [showMap, setShowMap] = useState(true)
  const { setShowPanel, showPanel } = useContext(MapContext)

  const toDesktop = useMediaPredicate(mediaToDesktop)

  return (
    <section className={`control-ui ${!showPanel && 'is-hidden'}`}>
      {toDesktop && (
        <>
          <button
            className="arrow-button"
            onClick={() => setShowPanel(!showPanel)}
          >
            <Icon svg={ArrowDown} size="1rem" />
          </button>
          <header className="tabs">
            <Tab
              text="Mapa"
              isActive={showMap}
              onClick={() => {
                setShowMap(true)
                setShowPanel(true)
              }}
            />
            <Tab
              text="Ruta"
              isActive={!showMap}
              onClick={() => {
                setShowMap(false)
                setShowPanel(true)
              }}
            />
          </header>
        </>
      )}
      <div className="container">
        <StepsPanel
          showMap={showMap}
          setShowPanel={setShowPanel}
          colors={colors}
          route={route}
        />
        <SearchPanel
          showMap={showMap}
          handleSetRoute={handleSetRoute}
          handleShowPanel={setShowPanel}
        />
      </div>
    </section>
  )
}

export default ControlUi
