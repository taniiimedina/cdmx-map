import { useContext } from 'react'
import { MapContext } from '../context/MapContext'
import { clearCoords } from '../utils/clear-coords'

const Step = ({ name, line, color, coords, setShowPanel }) => {
  const { setMapCoords } = useContext(MapContext)

  const setCoordsStep = () => {
    setShowPanel(false)
    setMapCoords(clearCoords(coords), 10)
  }

  return (
    <li className="step" style={{ '--color': color }} onClick={setCoordsStep}>
      <div>
        <p className="name">{name}</p>
        <p className="subtitle">{line}</p>
      </div>
    </li>
  )
}

export default Step
