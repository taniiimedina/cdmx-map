import { Marker, Tooltip } from 'react-leaflet'
import { clearCoords } from '../utils/clear-coords'

const MarkerFeatured = ({ route, element, title, color }) => {
  return route ? (
    <Marker position={clearCoords(element.coords)}>
      <Tooltip direction="top" permanent>
        <div
          className="tooltip-container featured"
          style={{ '--color': color }}
        >
          {title} - {element.name}
        </div>
      </Tooltip>
    </Marker>
  ) : null
}

export default MarkerFeatured
