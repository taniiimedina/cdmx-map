import SearchInput from './SearchInput'
import { useContext } from 'react'
import { MapContext } from '../context/MapContext'
import CheckboxContainer from './CheckboxContainer'

const SearchPanel = ({ handleSetRoute, showMap }) => {
  const {
    initialLocation,
    finalLocation,
    setInitialLocation,
    setFinalLocation,
  } = useContext(MapContext)

  return (
    <form className={`search-panel ${!showMap && 'hidden'}`}>
      <SearchInput
        data={initialLocation}
        showMap={showMap}
        label="Estación origen"
        setValueFunction={setInitialLocation}
        placeholder="Tacubaya"
      />
      <SearchInput
        data={finalLocation}
        showMap={showMap}
        label="Estación destino"
        setValueFunction={setFinalLocation}
        placeholder="Necaxa"
      />
      <CheckboxContainer />
      <button className="button" onClick={handleSetRoute}>
        ¡Genera tu ruta!
      </button>
    </form>
  )
}

export default SearchPanel
