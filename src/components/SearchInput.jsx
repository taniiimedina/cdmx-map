import debounce from 'lodash.debounce'
import { useContext, useEffect, useRef, useState } from 'react'
import uniqid from 'uniqid'
import { MapContext } from '../context/MapContext'

const SearchInput = ({ setValueFunction, label, placeholder }) => {
  const [showResults, setShowResults] = useState(false)
  const [filter, setFilter] = useState('')
  const { mainData, metroChecked, setInitialLocation, setFinalLocation } =
    useContext(MapContext)
  const input = useRef()

  const handleShowResults = debounce((event) => {
    setFilter(event.target.value)
    if (input.current.value === '') setShowResults(false)
    else setShowResults(true)
  }, 150)

  const handleSetValue = (element) => {
    setValueFunction(element)
    input.current.value = element
    hideResults()
  }

  const hideResults = () => setShowResults(false)

  const hideResultsFromEsc = (event) => {
    if (event.key === 'Escape') {
      event.preventDefault()
      hideResults()
    }
  }

  const hideResultsFromClickOutside = (event) => {
    if (!event.target.contains(input.current)) hideResults()
  }

  useEffect(() => {
    input.current.value = ''
    setFinalLocation('')
    setInitialLocation('')
  }, [metroChecked])

  useEffect(() => {
    window.addEventListener('keydown', hideResultsFromEsc)
    window.addEventListener('click', hideResultsFromClickOutside)

    return () => {
      window.removeEventListener('keydown', hideResultsFromEsc)
      window.removeEventListener('click', hideResultsFromClickOutside)
    }
  }, [])

  const formatString = (string) =>
    string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()

  return (
    <div className="search-input">
      <label className="label-container">
        <span className="label">{label}</span>
        <input
          placeholder={placeholder}
          className="input"
          ref={input}
          type="search"
          onChange={handleShowResults}
        />
      </label>
      {showResults && filter.length > 0 && (
        <ul className="list">
          {mainData.stations
            .filter(
              (station) =>
                station.includes(formatString(filter)) || filter === ''
            )
            .slice(0, 5)
            .map((station) => (
              <li
                key={uniqid()}
                className="item"
                onClick={() => handleSetValue(station)}
              >
                {station}
              </li>
            ))}
        </ul>
      )}
    </div>
  )
}

export default SearchInput
