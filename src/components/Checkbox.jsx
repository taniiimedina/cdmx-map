import Icon from './Icon'

const Checkbox = ({ label, value, checked, onClick, icon }) => {
  return (
    <label className={`checkbox ${checked && 'is-checked'}`} onClick={onClick}>
      <input
        className="input"
        type="radio"
        name="transport"
        value={value}
        checked={checked}
        readOnly
      />
      <Icon svg={icon} size="1rem" />
      <span className="text">{label}</span>
    </label>
  )
}

export default Checkbox
