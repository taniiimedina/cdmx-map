import Checkbox from './Checkbox'
import { useContext } from 'react'
import { MapContext } from '../context/MapContext'
import { Metro } from '../static/icons/Metro'
import { Metrobus } from '../static/icons/Metrobus'

const CheckboxContainer = () => {
  const { metroChecked, setMetroChecked } = useContext(MapContext)
  return (
    <div>
      <p className="label">Medio de transporte</p>
      <div className="checkbox-container">
        <Checkbox
          label="Metro"
          value="metro"
          checked={metroChecked}
          onClick={() => setMetroChecked(true)}
          icon={Metro}
        />
        <Checkbox
          label="Metrobus"
          value="metrobus"
          checked={!metroChecked}
          onClick={() => setMetroChecked(false)}
          icon={Metrobus}
        />
      </div>
    </div>
  )
}

export default CheckboxContainer
