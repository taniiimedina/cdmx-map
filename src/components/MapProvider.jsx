import { useEffect, useState } from 'react'
import { MapContext } from '../context/MapContext'
import { metroStations, metrobusStations } from '../data/stations'

const MapProvider = ({ children }) => {
  // Refer locations
  const [initialLocation, setInitialLocation] = useState('')
  const [finalLocation, setFinalLocation] = useState('')

  // Map
  const [mapCoords, setMapCoords] = useState([19.424954, -99.142845, 0])

  // App UI
  const [metroChecked, setMetroChecked] = useState(true)
  const [showPanel, setShowPanel] = useState(true)

  // Data
  const [metroData, setMetroData] = useState([])
  const [metrobusData, setMetrobusData] = useState([])
  const [mainData, setMainData] = useState([])

  // Config
  const isDev = process.env.REACT_APP_ENVIRONMENT === 'DEV'

  const handleFetchData = (setDataFunction, data, env) => {
    if (isDev) {
      setDataFunction(data)
    } else {
      fetch(`${process.env.REACT_APP_PROD_URL}/${env}/stations`)
        .then((result) => result.json())
        .then((result) => {
          setDataFunction(result)
        })
    }
  }

  useEffect(() => {
    handleFetchData(
      setMetroData,
      metroStations,
      process.env.REACT_APP_METRO_ROUTE
    )
    handleFetchData(
      setMetrobusData,
      metrobusStations,
      process.env.REACT_APP_METROBUS_ROUTE
    )
  }, [])

  useEffect(() => {
    setMainData(metroChecked ? metroData : metrobusData)
  }, [metroChecked, metroData, metrobusData])

  return (
    <MapContext.Provider
      value={{
        initialLocation,
        finalLocation,
        metroChecked,
        setMetroChecked,
        setInitialLocation,
        setFinalLocation,
        mapCoords,
        setMapCoords,
        showPanel,
        setShowPanel,
        mainData,
      }}
    >
      {children}
    </MapContext.Provider>
  )
}

export default MapProvider
