import PropTypes from 'prop-types'

const Icon = ({ svg, color, size }) => {
  return (
    <svg
      className="icon"
      width={size}
      height={size}
      viewBox="0 0 16 16"
      fill={color}
      xmlns="http://www.w3.org/2000/svg"
    >
      {svg}
    </svg>
  )
}

Icon.propTypes = {
  svg: PropTypes.node.isRequired,
  size: PropTypes.string,
  className: PropTypes.string,
  color: PropTypes.string,
}

Icon.defaultProps = {
  size: '1.25em',
  className: '',
  color: 'currentColor',
}

export default Icon
