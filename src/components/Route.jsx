import { CircleMarker, Polyline, Tooltip, Popup } from 'react-leaflet'
import uniqid from 'uniqid'
import { clearCoords } from '../utils/clear-coords'
import { getLastStep } from '../utils/get-last-step'
import MarkerFeatured from './MarkerFeatured'

const Route = ({ route, colors }) => {
  const createPolyline = (array) => {
    if (!array) return
    const polylineArray = []
    array.map((element) => polylineArray.push(clearCoords(element.coords)))
    return polylineArray
  }

  return (
    <>
      {route.length > 0 && (
        <MarkerFeatured
          route={route}
          title="Estación origen"
          element={route[0].steps[0]}
          color="var(--blue)"
        />
      )}

      {route.map((path, index) => (
        <Polyline
          key={uniqid()}
          pathOptions={{ color: colors[index] }}
          positions={createPolyline(path.steps)}
        />
      ))}

      {/* No renderiza el primer y último */}
      {route.map((path, index) =>
        path.steps.map(
          (element) =>
            element !== route[0].steps[0] &&
            element !== getLastStep(route) && (
              <CircleMarker
                key={uniqid()}
                center={clearCoords(element.coords)}
                pathOptions={{ color: colors[index] }}
                radius={10}
              >
                <Tooltip direction="top" permanent>
                  <div className="tooltip-container">
                    <p>{element.name}</p>
                    <p className="subtitle">{path.line}</p>
                  </div>
                </Tooltip>

                <Popup>
                  <p>{element.name}</p>
                  <p className="subtitle">{path.line}</p>
                </Popup>
              </CircleMarker>
            )
        )
      )}
      {route.length > 0 && (
        <MarkerFeatured
          route={route}
          title="Estación destino"
          element={getLastStep(route)}
          color="#23A010"
        />
      )}
    </>
  )
}

export default Route
