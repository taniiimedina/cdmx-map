const Tab = ({ onClick, text, isActive }) => {
  return (
    <button className={`tab ${isActive && 'is-active'}`} onClick={onClick}>
      {text}
    </button>
  )
}

export default Tab
