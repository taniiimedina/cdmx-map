import { useMapEvents } from 'react-leaflet'

const ZoomLevel = () => {
  const mapEvents = useMapEvents({
    zoomend: () => {
      document.body.classList[mapEvents.getZoom() >= 14 ? 'add' : 'remove'](
        'show-tooltips'
      )
    },
  })

  return null
}

export default ZoomLevel
