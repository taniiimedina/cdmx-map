import { useMap } from 'react-leaflet'

const UpdateMapView = ({ coords, zoom = 14 }) => {
  const map = useMap()
  map.setView(coords, zoom)
  return null
}

export default UpdateMapView
