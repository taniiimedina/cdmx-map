import Logo from '../static/images/logo.svg'

const PoweredByBunsan = () => {
  return (
    <a href="https://www.bunsan.io/" target="_blank" className="powered-by">
      <span>Powered by</span>
      <img className="logo" src={Logo} alt="Logo Bunsan" />
    </a>
  )
}

export default PoweredByBunsan
