import Step from './Step'
import uniqid from 'uniqid'
import WalkingGraphic from '../static/images/walking.svg'
import { useMediaPredicate } from 'react-media-hook'
import { mediaFromDesktop, mediaToDesktop } from '../utils/mediaquery'

const StepsPanel = ({ route, colors, setShowPanel, showMap }) => {
  const toDesktop = useMediaPredicate(mediaToDesktop)
  const fromDesktop = useMediaPredicate(mediaFromDesktop)

  return (
    <div className={showMap && toDesktop && 'hidden'}>
      {route.length > 0 ? (
        <aside className="steps-panel">
          {fromDesktop && (
            <p className="title">
              <strong>Tu ruta</strong>
            </p>
          )}
          <ul className="steps">
            {route.map((element, index) =>
              element.steps.map((step) => (
                <Step
                  setShowPanel={setShowPanel}
                  coords={step.coords}
                  color={colors[index]}
                  key={uniqid()}
                  line={element.line}
                  name={step.name}
                />
              ))
            )}
          </ul>
        </aside>
      ) : toDesktop ? (
        <div className="steps empty-state">
          <p className="title">
            Genera una ruta en el mapa <br /> para ver el paso a paso
          </p>
          <img className="image" src={WalkingGraphic} />
        </div>
      ) : null}
    </div>
  )
}

export default StepsPanel
