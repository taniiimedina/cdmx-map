import { MapContainer, TileLayer } from 'react-leaflet'
import { useContext, useEffect, useRef, useState } from 'react'
import { MapContext } from '../context/MapContext'
import { itinerary } from '../data/itinerary'
import Route from './Route'
import ControlUI from './ControlUI'
import UpdateMapView from './UpdateMapView'
import { clearCoords } from '../utils/clear-coords'
import ZoomLevel from './ZoomLevel'
import PoweredByBunsan from './PoweredByBunsan'

const Map = () => {
  const {
    initialLocation,
    finalLocation,
    mapCoords,
    setShowPanel,
    metroChecked,
    setMapCoords,
  } = useContext(MapContext)
  const [route, setRoute] = useState([])
  const map = useRef()

  const colors = [
    'var(--red)',
    'var(--orange)',
    'var(--green)',
    'var(--violet)',
    'var(--blue-alt)',
    'var(--red)',
    'var(--orange)',
    'var(--green)',
    'var(--violet)',
    'var(--blue-alt)',
  ]

  const handleSetRoute = (event) => {
    event.preventDefault()
    if (!initialLocation || !finalLocation) {
      alert('Debes agregar un origen y un destino')
      return
    }

    if (initialLocation === finalLocation) {
      alert('Las estaciones no pueden ser la misma')
      return
    }

    let temp = []

    if (process.env.REACT_APP_ENVIRONMENT === 'DEV') {
      itinerary.itinerary.map((line) => temp.push(line))
      handleCreateNewRoute(temp)
    } else {
      fetch(
        `${process.env.REACT_APP_PROD_URL}/${
          metroChecked
            ? process.env.REACT_APP_METRO_ROUTE
            : process.env.REACT_APP_METROBUS_ROUTE
        }/route?origin=${initialLocation}&dest=${finalLocation}`
      )
        .then((result) => result.json())
        .then((result) => {
          result.itinerary.forEach((line) => {
             temp.push(line)
          })
        }).then(() => handleCreateNewRoute(temp))
      }
  }

  const handleCreateNewRoute = (array) => {
    if (!array) return
    setRoute(array)
    setMapCoords(clearCoords(array[0].steps[0].coords))
    setShowPanel(false)
  }

  useEffect(() => {
    document.body.classList.add('show-tooltips')
  }, [map])

  return (
    <>
      <MapContainer
        ref={map}
        center={mapCoords}
        zoom={14}
        scrollWheelZoom={true}
      >
        <TileLayer
          url="https://api.mapbox.com/styles/v1/jopzik/cl4k9wwsb000414o8hsdypu1c/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoiam9wemlrIiwiYSI6ImNsNGs5dTQ1MjBmcDEzanAweWloMGk0cHYifQ.HpWeVuJf0FbJ5_FFHMqV5Q"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        <Route route={route} colors={colors} />
        <UpdateMapView coords={mapCoords} />
        <ZoomLevel />
      </MapContainer>

      <PoweredByBunsan />
      <ControlUI
        colors={colors}
        route={route}
        handleSetRoute={handleSetRoute}
      />
    </>
  )
}

export default Map
